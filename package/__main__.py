from functions import creating_a_sequence
from functions import find_max


commands = {"последовательность": creating_a_sequence,
            "максимум": find_max}


def reenter():
    while (command := input("Введите команду ")) != "end":
        if command in commands:
            try:
                print(commands[command](int(input("Введите n "))))
            except ValueError:
                print("Введено некоректное значение")
        else:
            print("Такой команды нет")


if __name__ == "__main__":
    reenter()
