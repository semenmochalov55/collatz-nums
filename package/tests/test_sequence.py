from ..functions import creating_a_sequence
from .test_find import input_data


def test_sequence_more_than_0():
    assert creating_a_sequence(5) == [5, 16, 8, 4, 2, 1], "Проверка работы с корректными значениями"
    assert creating_a_sequence(2) == [2, 1]


def test_sequence_single(input_data):
    assert creating_a_sequence(input_data) == [1], "Проверка при возврате единственного числа"


def test_sequence_not_correct():
    assert creating_a_sequence(0) == "Введено некоректное значение", "Проверка работы с числами < 1"


def test_sequence_big_nums():
    assert creating_a_sequence(10) == [10, 5, 16, 8, 4, 2, 1], "Проверка ф-ии на дистанции"
    assert creating_a_sequence(30) == [30, 15, 46, 23, 70, 35, 106, 53, 160, 80, 40, 20, 10, 5, 16, 8, 4, 2, 1]
