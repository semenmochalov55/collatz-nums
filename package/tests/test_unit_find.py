from unittest import TestCase
from ..functions import find_max


class FindingTest(TestCase):
    def test_finding_one_num(self):
        self.assertEqual(find_max(1), 1)

    def test_finding_many_nums(self):
        self.assertEqual(find_max(6), 16)
        