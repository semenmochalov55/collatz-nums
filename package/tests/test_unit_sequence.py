from unittest import TestCase
from ..functions import creating_a_sequence


class SequenceTest(TestCase):
    def test_sequence_more_than_0(self):
        self.assertEqual(creating_a_sequence(5), [5, 16, 8, 4, 2, 1])

    def test_sequence_not_correct(self):
        self.assertEqual(creating_a_sequence(0), "Введено некоректное значение")

    def test_sequence_big_nums(self):
        self.assertEqual(creating_a_sequence(30), [30, 15, 46, 23, 70, 35, 106, 53, 160, 80,
                                                   40, 20, 10, 5, 16, 8, 4, 2, 1])
