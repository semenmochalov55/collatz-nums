import pytest
from ..functions import find_max
from ..functions import creating_a_sequence


@pytest.fixture()
def input_data():
    return 1


tests_inp = [5, 6, 10]


@pytest.mark.parametrize('num', tests_inp)
def test_finding_many_nums(num):
    assert find_max(num) == 16, "Провекрка при коррекных числах"


def test_finding_one_num(input_data):
    assert find_max(input_data) == 1, "Провека при единственном числе"
